# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for the tool inventory system.
* This system is used to track the transactions, availability and the materials status in the storage area. 

### How do I get set up? ###

* Clone the repository into your web root directory
      - git clone https://ldoming@bitbucket.org/ldoming/tool-inventory-system.git
      - git clone git@bitbucket.org:ldoming/tool-inventory-system.git