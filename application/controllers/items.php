<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Items Class
*
* @author Lymuel Doming <lymuel.doming@gmail.com>
* This class contains all the CRUD method functionalities of items
*
*/

class items extends CI_Controller
{

	private $page_config = [
		'title'  => 'Items',
		'active' =>	'items'
	];

	private $layout 		= 'user_layout';
	private $account_type 	= 'user';


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('error', '<div class="error">Unauthorized to access page. Please login</div>');
			redirect(base_url('accounts'));
		}

		$this->account_type = $this->session->userdata('account_type');

		if ($this->account_type == 'admin')
		{
			$this->layout = 'admin_layout';
		}
		else if ($this->account_type == 'operator')
		{
			$this->layout = 'operator_layout';
		}
		$this->load->model('item');
		$this->load->model('category');
		$this->load->model('storage_area');

	}


	public function index()
	{	
		$url = base_url('items/?disabled=' .$this->input->get('disabled')
			. '&category_id=' .$this->input->get('category_id')
			. '&storage_id=' .$this->input->get('storage_id')
			. '&search='.$this->input->get('search'));

		$config = init_pagination($url, $this->item->retreiveItemLists('count',null,null, $this->input->get()), 10, 3);
		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;
		$page = (!is_numeric($page)) ? 1 : $page;
		$page = ($page > 0) ? ($page-1) * $config['per_page'] : 0;
		$this->page_config['results'] = $this->item->retreiveItemLists('data',$config['per_page'], $page, $this->input->get());
		$this->page_config['pagination'] = $this->pagination->create_links();
		$this->page_config['categories'] = $this->category->retreiveCategoryType();
		$this->page_config['storage'] = $this->storage_area->retreiveStorageType();
		$this->load->view($this->layout, ['view'=>'items/index','data' => $this->page_config]);
	}


	public function itemAdd()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('name', 'name', 'required|is_unique[items.name]');
			$this->form_validation->set_rules('category_id', 'category', 'required');
			$this->form_validation->set_rules('storage_id', 'description', 'required');
			$this->form_validation->set_rules('item_type', 'item type', 'required');
			$this->form_validation->set_rules('status', 'item status', 'required');
			$this->form_validation->set_rules('quantity', 'quantity', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				if($this->item->insertItemInfo($this->input->post()))
				{
					$this->session->set_flashdata('success', 'Item added successfully.');
					redirect(base_url('items'));
				}
				$this->session->set_flashdata('error', 'Error adding item. Please try again!');
			}
		}
		$this->page_config['categories'] = $this->category->retreiveCategoryType();
		$this->page_config['storage'] = $this->storage_area->retreiveStorageType();
		$this->page_config['title'] = 'Add Item';
		$this->load->view($this->layout, ['view'=>'items/itemAdd','data' => $this->page_config]);
	}

	public function itemUpdate($id = null)
	{
		$this->checkVariableNotNull($id);
		if($this->input->post())
		{
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('category_id', 'category', 'required');
			$this->form_validation->set_rules('storage_id', 'description', 'required');
			$this->form_validation->set_rules('item_type', 'item type', 'required');
			$this->form_validation->set_rules('status', 'item status', 'required');
			$this->form_validation->set_rules('quantity', 'quantity', 'required');
			$this->form_validation->set_rules('description', 'description', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				if($this->item->updateItemInfo($this->input->post(),$id))
				{
					$this->session->set_flashdata('success', 'Item updated successfully.');
					redirect(base_url('items'));
				}
				$this->session->set_flashdata('error', 'Error updating item. Please try again!');
			}
		}
		$this->page_config['results'] = $this->item->retreiveItemDetail($id);
		$this->page_config['categories'] = $this->category->retreiveCategoryType();
		$this->page_config['storage'] = $this->storage_area->retreiveStorageType();
		$this->page_config['title'] = 'Update Item';
		$this->load->view($this->layout, ['view'=>'items/itemUpdate','data' => $this->page_config]);
	}

	public function itemDelete($id = null, $type = 1)
	{
		$this->checkVariableNotNull($id);

		if ($type != 1 && $type != 0)
		{
			$type = 1;
		}
		$result = $this->item->itemDelete($id,$type);
		$type = ($type == 1) ? 'disabled' : 'enabled';
		if ($result)
		{
			$this->session->set_flashdata('success', 'Item '.$type. ' successfully!');
		}
		else
		{
			$this->session->set_flashdata('error', 'Error to ' .$type. ' item! Please try again.');
		}
		return redirect(base_url('items'));

	}


####################################################################################
# Private methods below
####################################################################################

	private function checkVariableNotNull($variable = null, $name = 'ID')
	{
		if (!$variable)
		{
			$this->session->set_flashdata('error', $name . ' may not specified or not exist. Please try again');
			return redirect(base_url('items'));
		}
		return true;
	}



}