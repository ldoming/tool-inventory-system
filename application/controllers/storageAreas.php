<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Categories Class
*
* @author Lymuel Doming <lymuel.doming@gmail.com>
* This class contains all the CRUD method functionalities of items
*
*/

class storageAreas extends CI_Controller
{

	private $page_config = [
		'title'  => 'Storage Area',
		'active' =>	'storageAreas'
	];

	private $layout 		= 'user_layout';
	private $account_type 	= 'user';


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('error', '<div class="error">Unauthorized to access page. Please login</div>');
			redirect(base_url('accounts'));
		}

		$this->account_type = $this->session->userdata('account_type');

		if ($this->account_type == 'admin')
		{
			$this->layout = 'admin_layout';
		}
		else if ($this->account_type == 'operator')
		{
			$this->layout = 'operator_layout';
		}
		$this->load->model('storage_area');

	}


	public function index()
	{	
		$this->page_config['results'] = $this->storage_area->retreiveStorageAreaLists();
		$this->load->view($this->layout, ['view'=>'storageAreas/index','data' => $this->page_config]);
	}


	public function storageAdd()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('name', 'Name', 'required|is_unique[storage.name]');
			$this->form_validation->set_rules('delete_flag', 'Status', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$result = $this->storage_area->insertStorageAreaInfo($this->input->post());
				if($result){
					$this->session->set_flashdata('success', 'Storage info added successfully.');
					return redirect(base_url('storageAreas'));
				}
				$this->session->set_flashdata('error', 'Error adding storage info. Please try again!');
			}
		}
		$this->page_config['title'] = 'Add Storage Area Info';
		$this->load->view($this->layout, ['view'=>'storageAreas/storageAdd','data' => $this->page_config]);
	}


}