<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class accounts extends CI_Controller
{

	private $page_config = [
		'title'  => 'Accounts',
		'active' => 'accounts'
	];

	private $layout         = 'main_layout';
	private $account_type   = 'user';

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') && $this->router->fetch_method() == 'index')
		{
			redirect(base_url('pages/dashboard'));
		}

		if($this->session->userdata('logged_in'))
		{
			$this->account_type = $this->session->userdata('account_type');

			if ($this->account_type == 'admin')
			{
				$this->layout = 'admin_layout';
			}
			else if ($this->account_type == 'operator')
			{
				$this->layout = 'operator_layout';
			}
		}

		if(!$this->session->userdata('logged_in') && $this->router->fetch_method() == 'accountLists')
		{
			$this->session->set_flashdata('error', '<div class="error">Unauthorized to access page. Please login</div>');
			redirect(base_url('accounts'));
		}
		$this->load->model('account');
	}

	public function index()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$result = $this->account->accountLogin($this->input->post());
				if($result === true)
				{
					redirect(base_url('pages/dashboard'));
				}
				else if ($result == 'Disabled')
				{
					$this->session->set_flashdata('error', '<div class="error">Account is disabled please contanct site Admin!</div>');
				}
				else
				{
					$this->session->set_flashdata('error', '<div class="error">Email and Password not match! Please try again!</div>');
				}
				redirect(base_url('accounts'));
			}
		}
		$this->load->view('main_layout',['view'=>'accounts/login','data' => null]);
	}

	public function register()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules('fullname', 'Full Name', 'required|is_unique[accounts.name]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[accounts.email]');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[confirmpassword]');
			$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				if($this->account->registerAccount($this->input->post()))
				{
					$this->session->set_flashdata('error', 'Account added successfully!');
				}
				else
				{
					$this->session->set_flashdata('error', 'Error in adding accounts! Please try again later!');
				}
				redirect(base_url('accounts'));
			}
		}
		$this->load->view('main_layout',['view'=>'accounts/register','data' => null]);
	}


	public function mailForResetPassword()
	{
		if($this->input->post('emailaddress'))
		{
			$email = $this->input->post('emailaddress');
			$token = md5($this->input->post('emailaddress').date('Y-m-d H:i:s'));
			if(!valid_email($email))
			{
				$this->session->set_flashdata('error', 'Please input correct email address');
				redirect(base_url('accounts/mailForResetPassword'));
			}
			$result = $this->account->checkAccountIfExist($email);
			if($result)
			{
				$this->account->insertTokenInformation($email, $token);
				$this->session->set_flashdata('error', 'Email sent successfully!');
				redirect(base_url('accounts'));
			}
			$this->session->set_flashdata('error', 'Email address doesn\'t exist in database!');
			redirect(base_url('accounts/mailForResetPassword'));
		}
		$this->load->view('main_layout',['view'=>'accounts/send_email_reset_password','data' => null]);
	}

	public function resetPassword($token = null){
		if(!$token){
			redirect(base_url('accounts'));
		}
		$result = $this->account->checkTokenIfExist($token);

		if(!$result)
		{
			$this->session->set_flashdata('error', 'Reset token not available!');
			redirect(base_url('accounts'));
		}

		if($this->input->post())
		{
			$this->form_validation->set_rules('password', 'Password', 'required|matches[confirmpassword]');
			$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$this->account->resetPassword($this->input->post('password'),$result);
				if(!$this->account->updateTokenToAccessed($token))
				{
					$this->session->set_flashdata('error', 'Error updating token');
					redirect(base_url('accounts'));
				}
				else
				{
					$this->session->set_flashdata('error', 'Password updated successfully!');
					redirect(base_url('accounts'));
				}
			}
		}
		$this->load->view('main_layout',['view'=>'accounts/reset_password','data' => ['token' => $token]]);
	}


	###############################################################
	#  METHODS LOGIN REQUIRED
	###############################################################

	public function accountLists()
	{
		$url = base_url('accounts/accountLists/?disabled=' .$this->input->get('disabled')
			. '&user=' .$this->input->get('user')
			. '&operator=' .$this->input->get('operator')
			. '&admin=' .$this->input->get('admin')
			. '&search='.$this->input->get('search'));
		$config = init_pagination($url, $this->account->retreiveAccounts('count',null,null, $this->input->get()), 10, 3);
		$page = ($this->input->get('page')) ? $this->input->get('page') : 1;
		$page = (!is_numeric($page)) ? 1 : $page;
		$page = ($page > 0) ? ($page-1) * $config['per_page'] : 0;
		$this->page_config['results'] = $this->account->retreiveAccounts('data',$config['per_page'], $page, $this->input->get());
		$this->page_config['pagination'] = $this->pagination->create_links();
		$this->load->view($this->layout, ['view'=>'accounts/accountLists','data' => $this->page_config]);
	}

	public function accountEdit($id = null)
	{
		if ($this->input->post())
		{
			$this->form_validation->set_rules('fullname', 'Full Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('account_type', 'Account Type', 'required');
			$this->form_validation->set_rules('delete_flag', 'Account Status', 'required');
			$this->form_validation->set_rules('firstname', 'Firstname', 'required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required');
			$this->form_validation->set_rules('contact_number', 'Contact Number', 'required|numeric|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('birthday', 'Birthday', 'required');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$result = $this->account->accountUpdate($this->input->post());
				if ($result)
				{
					$this->session->set_flashdata('success', 'Account updated successfully.');
					return redirect(base_url('accounts/accountLists'));
				}
				$this->session->set_flashdata('error', 'Error updating account. Please try again!');
			}
		}
		if ($id)
		{
			$result = $this->account->checkAccountIfExistById($id);
			if ($result)
			{
				$this->page_config['result'] = $result;
				$this->page_config['title'] = 'Account Update';
				return $this->load->view($this->layout, ['view'=>'accounts/accountEdit','data' => $this->page_config]);
			}
		}
		$this->session->set_flashdata('error', 'ID may not specified or not exist. Please try again');
		redirect(base_url('accounts/accountLists'));
	}

	public function accountAdd()
	{
		if ($this->input->post())
		{
			$this->form_validation->set_rules('fullname', 'Full Name', 'required|is_unique[accounts.name]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[accounts.email]');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
			$this->form_validation->set_rules('account_type', 'Account Type', 'required');
			$this->form_validation->set_rules('delete_flag', 'Account Status', 'required');
			$this->form_validation->set_rules('firstname', 'Firstname', 'required|is_unique[member_infos.firstname]');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required|is_unique[member_infos.lastname]');
			$this->form_validation->set_rules('contact_number', 'Contact Number', 'required|numeric|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('birthday', 'Birthday', 'required');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				$result = $this->account->accountAdd($this->input->post());
				if ($result)
				{
					$this->session->set_flashdata('success', 'Account added successfully.');
					return redirect(base_url('accounts/accountLists'));
				}
				$this->session->set_flashdata('error', 'Error adding account. Please try again!');
			}
		}
		return $this->load->view($this->layout, ['view'=>'accounts/accountAdd','data' => $this->page_config]);
	}

	public function accountDelete($id = null, $type = 1)
	{
		$this->checkVariableNotNull($id,'ID');

		if ($type != 1 && $type != 0)
		{
			$type = 1;
		}
		$result = $this->account->accountDelete($id,$type);
		$type = ($type == 1) ? 'disabled' : 'enabled';
		if ($result)
		{
			$this->session->set_flashdata('success', 'Account ' .$type. ' successfully!');
			return redirect(base_url('accounts/accountLists'));
		}
		else
		{
			$this->session->set_flashdata('error', 'Error to ' .$type. ' account! Please try again.');
			return redirect(base_url('accounts/accountLists'));
		}
	}

	public function accountDetails($id = null)
	{
		$this->checkVariableNotNull($id,'ID');
		$this->page_config['results'] = $this->account->retreiveAccountDetails($id);
		$this->page_config['title'] = 'Account Details';
		return $this->load->view($this->layout, ['view'=>'accounts/accountDetails','data' => $this->page_config]);
	}

	public function logout()
	{
		if($this->account->logout())
		{
			$this->session->set_flashdata('error', '<div style="color:green;">Logout successfully!</div>');
			redirect(base_url('accounts'));
		}
	}



####################################################################################
# Private methods below
####################################################################################

	private function checkVariableNotNull($variable = null, $name = 'ID')
	{
		if (!$variable)
		{
			$this->session->set_flashdata('error', $name . ' may not specified or not exist. Please try again');
			return redirect(base_url('accounts/accountLists'));
		}
		return true;
	}


}