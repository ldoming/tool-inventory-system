<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Categories Class
*
* @author Lymuel Doming <lymuel.doming@gmail.com>
* This class contains all the CRUD method functionalities of items
*
*/

class categories extends CI_Controller
{

	private $page_config = [
		'title'  => 'Categories',
		'active' =>	'categories'
	];

	private $layout 		= 'user_layout';
	private $account_type 	= 'user';


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('error', '<div class="error">Unauthorized to access page. Please login</div>');
			redirect(base_url('accounts'));
		}

		$this->account_type = $this->session->userdata('account_type');

		if ($this->account_type == 'admin')
		{
			$this->layout = 'admin_layout';
		}
		else if ($this->account_type == 'operator')
		{
			$this->layout = 'operator_layout';
		}
		$this->load->model('category');
	}


	public function index()
	{	
		$this->page_config['results'] = $this->category->retreiveCategoryLists();
		$this->load->view($this->layout, ['view'=>'categories/index','data' => $this->page_config]);
	}


	public function categoryAdd()
	{
		if ($this->input->post())
		{
			$this->form_validation->set_rules('name', 'Name', 'required|is_unique[categories.name]');
			$this->form_validation->set_rules('delete_flag', 'Status', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			if ($this->form_validation->run() == TRUE)
			{
				if($this->category->insertCategoryInfo($this->input->post())){
					$this->session->set_flashdata('success', 'Category info added successfully.');
					return redirect(base_url('categories'));
				}
				$this->session->set_flashdata('error', 'Error adding category info. Please try again!');
			}
		}

		$this->page_config['title'] = 'Add Category Info';
		$this->load->view($this->layout, ['view'=>'categories/categoryAdd','data' => $this->page_config]);
	}


}