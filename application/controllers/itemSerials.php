<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Categories Class
*
* @author Lymuel Doming <lymuel.doming@gmail.com>
* This class contains all the CRUD method functionalities of items
*
*/

class itemSerials extends CI_Controller
{

	private $page_config = [
		'title'  => 'Item Serial Numbers',
		'active' =>	'itemSerials'
	];

	private $layout 		= 'user_layout';
	private $account_type 	= 'user';


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('error', '<div class="error">Unauthorized to access page. Please login</div>');
			redirect(base_url('accounts'));
		}

		$this->account_type = $this->session->userdata('account_type');

		if ($this->account_type == 'admin')
		{
			$this->layout = 'admin_layout';
		}
		else if ($this->account_type == 'operator')
		{
			$this->layout = 'operator_layout';
		}

	}


	public function index()
	{	
		$this->load->view($this->layout, ['view'=>'itemSerials/index','data' => $this->page_config]);
	}


	public function add()
	{
		$this->load->view($this->layout, ['view'=>'itemSerials/add','data' => $this->page_config]);
	}


}