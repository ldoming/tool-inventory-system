<div class="row">
	<div class="col-md-6">
		<?php echo form_open(base_url('storageAreas/storageAdd'),['role'=>'form']);?>
			<div class="form-group">
				<label for="name">Name</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Storage Name','type'=>'text','name'=>'name','value'=> set_value('name')]);?>
				<?php echo form_error('name', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="delete_flag">Category Status</label>
				<br/>
				<?php
					$enabled = array(
						'name'		=> 'delete_flag',
						'id'		=> 'delete_flag',
						'value'		=> 0,
						'checked'	=> TRUE
					);
					$disabled = array(
						'name'		=> 'delete_flag',
						'id'		=> 'delete_flag',
						'value'		=> 1,
					);
					echo form_radio($enabled) . '  Enabled &nbsp;&nbsp;&nbsp;';
					echo form_radio($disabled) . '  Disabled ';
				?>
				<?php echo form_error('delete_flag', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				<br/>
				<?php echo form_textarea(['name' => 'description', 'class' => 'form-control', 'style' => 'resize:none;', 'value'=> set_value('description')]);?>
				<?php echo form_error('description', '<div class="error">', '</div>'); ?>
			</div>

			<button type="submit" class="btn btn-success">Submit</button>
		<?php echo form_close();?>
	</div>
</div>
