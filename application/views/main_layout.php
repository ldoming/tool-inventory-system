<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>Lymuel Doming::CI Review</title>
        <!-- Description, Keywords and Author -->
        <meta name="description" content="CI Review">
        <meta name="keywords" content="CI Review">
        <meta name="author" content="Lymuel Doming">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Styles -->
        <!-- Bootstrap CSS -->
        <?php echo link_tag('public/css/bootstrap.min.css');?>
        <!-- Custom CSS -->
        <?php echo link_tag('public/css/style.css');?>
        
        <link rel="shortcut icon" href="#">
    </head>
    
    <body>
        <style>
            .error{
                color:red;
            }
        </style>
        <?php echo $this->load->view($view,$data);?>
    </body> 
        <!-- Javascript files -->
        <!-- jQuery -->
        <script src="<?php echo base_url('public');?>/js/jquery.js"></script>
        <!-- Bootstrap JS -->
        <script src="<?php echo base_url('public');?>/js/bootstrap.min.js"></script>
        <!-- Respond JS for IE8 -->
        <script src="<?php echo base_url('public');?>/js/respond.min.js"></script>
        <!-- HTML5 Support for IE -->
        <script src="<?php echo base_url('public');?>/js/html5shiv.js"></script>
</html>