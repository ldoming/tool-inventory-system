<div class="row">
	<div class="col-md-6">
		<?php echo form_open(base_url('items/itemAdd'),['role'=>'form']);?>
			<div class="form-group">
				<label for="name">Name</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Name','type'=>'text','name'=>'name','value'=> set_value('fullname')]);?>
				<?php echo form_error('name', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="category_id">Category</label>
				<br/>
				<?php 
					echo form_dropdown('category_id', $data['categories'], set_value('category_id'));
				?>
				<a href="<?php echo base_url('categories/categoryAdd');?>">
					<button type="button" class="btn btn-info btn-xs">
						<i class="fa fa-plus"></i> Add Category Info
					</button>
				</a>
				<?php echo form_error('category_id', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="storage_id">Storage Area</label>
				<br/>
				<?php
					echo form_dropdown('storage_id', $data['storage'], set_value('storage_id'));
				?>
				<a href="<?php echo base_url('storageAreas/storageAdd');?>">
					<button type="button" class="btn btn-info btn-xs">
						<i class="fa fa-plus"></i> Add Storage Info
					</button>
				</a>
				<?php echo form_error('storage_id', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="item_type">Item Type</label>
				<br/>
				<?php
					$consumable = array(
						'name'		=> 'item_type',
						'id'		=> 'item_type',
						'value'		=> 0,
					);
					$returnable = array(
						'name'		=> 'item_type',
						'id'		=> 'item_type',
						'value'		=> 1,
					);
					echo form_radio($consumable) . '  Consumable &nbsp;&nbsp;&nbsp;';
					echo form_radio($returnable) . '  Returnable ';
				?>
				<?php echo form_error('item_type', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="status">Quantity Status</label>
				<br/>
				<?php
					$on_stock = array(
						'name'		=> 'status',
						'id'		=> 'status',
						'value'		=> 1,
						'checked'	=> TRUE
					);
					$out_of_stock = array(
						'name'		=> 'status',
						'id'		=> 'status',
						'value'		=> 0,
					);
					echo form_radio($on_stock) . '  Have stock/s &nbsp;&nbsp;&nbsp;';
					echo form_radio($out_of_stock) . '  Out of Stock ';
				?>
				<?php echo form_error('delete_flag', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="delete_flag">Item Status</label>
				<br/>
				<?php
					$enabled = array(
						'name'		=> 'delete_flag',
						'id'		=> 'delete_flag',
						'value'		=> 0,
						'checked'	=> TRUE
					);
					$disabled = array(
						'name'		=> 'delete_flag',
						'id'		=> 'delete_flag',
						'value'		=> 1,
					);
					echo form_radio($enabled) . '  Enabled &nbsp;&nbsp;&nbsp;';
					echo form_radio($disabled) . '  Disabled ';
				?>
				<?php echo form_error('delete_flag', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="quantity">Quantity</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Quantity','type'=>'text','name'=>'quantity','value'=> set_value('quantity')]);?>
				<?php echo form_error('quantity', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="image">Image</label>
				<?php echo form_upload(['name' => 'image']);?>
				<?php echo form_error('image', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				<br/>
				<?php echo form_textarea(['name' => 'description', 'class' => 'form-control', 'style' => 'resize:none;','value' => set_value('description')]);?>
				<?php echo form_error('description', '<div class="error">', '</div>'); ?>
			</div>

			<button type="submit" class="btn btn-success">Submit</button>
		<?php echo form_close();?>
	</div>
</div>
