<div class="container-fluid" style="margin-bottom:20px;">
	<?php echo form_open('items', ['method' => 'GET','class' =>'form-inline', 'role' => 'form']);?>
	<div class="row">
		<div class="col-md-6 text-left">
			<div class="checkbox">
				<label>
					<input type="checkbox" value="1" name="disabled" <?php echo ($this->input->get('disabled')) ? 'checked=checked':''?>/> Disabled&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
			</div>

			<div class="form-group">
				<?php 
					echo 'Category: '. form_dropdown('category_id', $data['categories'], $this->input->get('category_id'));
				?>
			</div>
			<div class="form-group">
				<?php 
					echo 'Storage: '. form_dropdown('storage_id', $data['storage'], $this->input->get('storage_id'));
				?>
			</div>
		</div>
		<div class="col-md-6 text-right">
			<div class="form-group">
				<label class="sr-only" for="exampleInputPassword2">Search Keyword</label>
				<input type="text" class="form-control" placeholder="Search Keyword" name="search" value='<?php echo ($this->input->get("search")) ? $this->input->get("search"):""; ?>' />
			</div>
			<button class="btn btn-success" type="submit">
				<i class="fa fa-search"></i> Search
			</button>
		</div>
	</div>
	<?php echo form_close();?>
	<hr/>
	<div class="row-fluid">
		<div class="span12">
			<div class="btn-group">
				<a href="<?php echo base_url('items/itemAdd');?>">
				 	<button class="btn btn-success" type="button">
				 		<i class="fa fa-plus"></i> Add
			 		</button>
		 		</a>
		 		<a href="<?php echo base_url('itemSerials/add');?>">
				 	<button class="btn btn-info" type="button">
				 		<i class="fa fa-tasks"></i> Add Serials
				 	</button>
			 	</a>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<td>Name</td>
					<td>Category</td>
					<td>Storage</td>
					<td>Item Type</td>
					<td>Status</td>
					<td>Quantity</td>
					<td align="center">Actions</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data['results'] as $value): ?>
				<tr <?php echo ($value->itemDeleteFlag == 1) ? 'style="background-color: rgba(215, 22, 24, 0.1);"' : '' ;?> >
					<td><?php echo $value->itemName; ?></td>
					<td><?php echo $value->categoryName; ?></td>
					<td><?php echo $value->storageName; ?></td>
					<td><?php echo item_type($value->item_type); ?></td>
					<td><?php echo item_status($value->status); ?></td>
					<td><?php echo $value->quantity; ?></td>
					<td align="center">
						<?php if($value->itemDeleteFlag == 0){?>
						<a href="<?php echo base_url('items/itemDelete/' .$value->itemId. '/1');?>" title="Disable" alt="Disable" onclick="return confirm('Are you sure you want to disable this item <?php echo $value->itemName;?>?');">
							<i class="fa fa-trash-o text-danger"></i>
						</a>
						<?php }else{?>
						<a href="<?php echo base_url('items/itemDelete/' .$value->itemId. '/0');?>" title="Enable" alt="Enable" onclick="return confirm('Are you sure you want to enable this item <?php echo $value->itemName;?>?');">
							<i class="fa fa-recycle text-success"></i>
						</a>
						<?php }?>
						&nbsp;&nbsp;
						<a href="<?php echo base_url('items/itemUpdate/' .$value->itemId);?>" title="Update" alt="Update">
							<i class="fa fa-pencil-square-o text-success"></i>
						</a>
						&nbsp;&nbsp;
						<a href="#" title="Add Serials" alt="Add Serials">
							<i class="fa fa-tasks text-info"></i>
						</a>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<nav class="text-center">
	  	<ul class="pagination pagination-centered">
			<li><a href="#"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
	  	</ul>
	</nav>
</div>