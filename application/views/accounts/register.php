<div class="out-container">
    <div class="login-page">
        <div class="container">
            <ul class="nav nav-tabs nav-justified">
                <li><a href="<?php echo base_url('accounts');?>"><i class="fa fa-sign-in"></i> Login</a></li>
                <li class="active"><a href="<?php echo base_url('accounts/register');?>"><i class="fa fa-pencil"></i> Register</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="register">
                    <!-- Register form -->
                    <?php echo form_open(base_url('accounts/register/'),['role'=>'form']);?>
                        <div class="form-group">
                            <label for="name">Full Name</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Full Name','type'=>'text','name'=>'fullname','value'=> set_value('fullname')]);?>
                            <?php echo form_error('fullname', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Email','type'=>'text','name'=>'email','value'=> set_value('email')]);?>
                            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Password','type'=>'password','name'=>'password','value'=> set_value('password')]);?>
                            <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="cpassword">Confirm Password</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Confirm Password','type'=>'password','name'=>'confirmpassword','value'=> set_value('confirmpassword')]);?>
                            <?php echo form_error('confirmpassword', '<div class="error">', '</div>'); ?>
                        </div>
                        <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                        <button type="reset" class="btn btn-black btn-sm">Reset</button>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>