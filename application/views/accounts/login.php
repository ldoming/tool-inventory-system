<div class="out-container">
    <div class="login-page">
        <div class="container">
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="<?php echo base_url('accounts');?>"><i class="fa fa-sign-in"></i> Login</a></li>
                <li><a href="<?php echo base_url('accounts/register');?>"><i class="fa fa-pencil"></i> Register</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="login">
                    <!-- Login Form-->
                    <div class="alert alert-warning">
                        <?php echo ($this->session->flashdata('error'))? $this->session->flashdata('error'):'Just <strong>Click "Submit"</strong> to visit the Dashboard';?>
                    </div>
                    <?php echo form_open(base_url('accounts/index'),['role'=>'form']);?>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Email Address','type'=>'text','name'=>'email','value'=> set_value('email')]);?>
                            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Password','type'=>'password','name'=>'password','value'=> set_value('password')]);?>
                            <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                        </div>
                        <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                        <button type="reset" class="btn btn-black btn-sm">Reset</button>
                        <?php echo nbs(2)?>
                        <a href="<?php echo base_url('accounts/mailForResetPassword');?>">Forget Password?</a>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>