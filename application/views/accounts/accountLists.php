<div class="container-fluid" style="margin-bottom:20px;">
	<?php echo form_open('accounts/accountLists', ['method' => 'GET','class' =>'form-inline', 'role' => 'form']);?>
	<div class="row">
		<div class="col-md-6 text-left">
			<div class="checkbox">
				<label>
					<input type="checkbox" value="1" name="disabled" <?php echo ($this->input->get('disabled')) ? 'checked=checked':''?>/> Disabled&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" value="1" name="user" <?php echo ($this->input->get('user')) ? 'checked=checked':''?>/> User&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" value="1" name="operator" <?php echo ($this->input->get('operator')) ? 'checked=checked':''?>/> Operator&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" value="1" name="admin" <?php echo ($this->input->get('admin')) ? 'checked=checked':''?>/> Administrator&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
			</div>
		</div>
		<div class="col-md-6 text-right">
			<div class="form-group">
				<label class="sr-only" for="exampleInputPassword2">Search Keyword</label>
				<input type="text" class="form-control" placeholder="Search Keyword" name="search" value='<?php echo ($this->input->get("search")) ? $this->input->get("search"):""; ?>' />
			</div>
			<button class="btn btn-success" type="submit">
				<i class="fa fa-search"></i> Search
			</button>
		</div>
	</div>
	<?php echo form_close();?>
	<hr/>
	<div class="row-fluid">
		<div class="span12">
			<div class="btn-group">
				<a href="<?php echo base_url('accounts/accountAdd');?>">
					<button class="btn btn-success" type="button">
						<i class="fa fa-plus"></i> Add
					</button>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<td>Name</td>
					<td>Email</td>
					<td>Account Type</td>
					<td>Contact #</td>
					<td>Birthday</td>
					<td>Gender</td>
					<td align="center">Actions</td>
				</tr>
			</thead>
			<tbody>
			<?php foreach($data['results'] as $value):?>
				<tr <?php echo ($value->delete_flag == 1) ? 'style="background-color: rgba(215, 22, 24, 0.1);"' : '' ;?> >
					<td>
						<a href="<?php echo base_url('accounts/accountDetails/'.$value->id);?>">
							<?php echo $value->name; ?>
						</a>
					</td>
					<td><?php echo $value->email; ?></td>
					<td><?php echo $value->account_type; ?></td>
					<td><?php echo $value->contact_number; ?></td>
					<td><?php echo date_to_words($value->birthday); ?></td>
					<td><?php echo gender($value->gender); ?></td>
					<td align="center">
						<?php if($value->delete_flag == 0){?>
						<a href="<?php echo base_url('accounts/accountDelete/' .$value->id);?>" title="Disable" alt="Disable" onclick="return confirm('Are you sure you want to disable account <?php echo $value->email;?>?');">
							<i class="fa fa-trash-o text-danger"></i>
						</a>
						<?php }else{?>
						<a href="<?php echo base_url('accounts/accountDelete/' .$value->id.'/en');?>" title="Enable" alt="Enable" onclick="return confirm('Are you sure you want to enable account <?php echo $value->email;?>?');">
							<i class="fa fa-recycle text-success"></i>
						</a>
						<?php }?>
						&nbsp;&nbsp;
						<a href="<?php echo base_url('accounts/accountEdit/' .$value->id);?>" title="Update" alt="Update">
							<i class="fa fa-pencil-square-o text-info"></i>
						</a>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<?php echo $this->load->view('elements/pagination');?>
</div>