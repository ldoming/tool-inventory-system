<div class="row">
	<div class="col-md-6">
		<?php echo form_open(base_url('accounts/accountAdd'),['role'=>'form']);?>
			<div class="form-group">
				<label for="name">Full Name</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Full Name','type'=>'text','name'=>'fullname','value'=> set_value('fullname')]);?>
				<?php echo form_error('fullname', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Email','type'=>'text','name'=>'email','value'=> set_value('email')]);?>
				<?php echo form_error('email', '<div class="error">', '</div>'); ?>
			</div>

			<div class="form-group">
				<label for="password">Password</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Password','type'=>'password','name'=>'password','value'=> set_value('password')]);?>
				<?php echo form_error('password', '<div class="error">', '</div>'); ?>
			</div>

			<div class="form-group">
				<label for="confirm_password">Confirm Password</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Confirm Password','type'=>'password','name'=>'confirm_password','value'=> set_value('confirm_password')]);?>
				<?php echo form_error('confirm_password', '<div class="error">', '</div>'); ?>
			</div>

			<div class="form-group">
				<label for="account_type">Account Type</label>
				<br/>
				<?php 
					$options = array(
						''  => '-- Please Select --',
						'user'		=> 'User',
						'operator'	=> 'Operator',
						'admin'		=> 'Administrator',
					);
					echo form_dropdown('account_type', $options, set_value('company_id_number'));
				?>
				<?php echo form_error('account_type', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="delete_flag">Account Status</label>
				<br/>
				<?php
					$enabled = array(
						'name'		=> 'delete_flag',
						'id'		=> 'delete_flag',
						'value'		=> 0,
					);
					$disabled = array(
						'name'		=> 'delete_flag',
						'id'		=> 'delete_flag',
						'value'		=> 1,
					);
					echo form_radio($enabled) . '  Enabled &nbsp;&nbsp;&nbsp;';
					echo form_radio($disabled) . '  Disabled ';
				?>
				<?php echo form_error('delete_flag', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="firstname">Firstname</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Firstname','type'=>'text','name'=>'firstname','value'=> set_value('firstname')]);?>
				<?php echo form_error('firstname', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="lastname">Lastname</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Lastname','type'=>'text','name'=>'lastname','value'=> set_value('lastname')]);?>
				<?php echo form_error('lastname', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="contact_number">Contact Number</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Contact Number','type'=>'text','name'=>'contact_number','value'=> set_value('contact_number')]);?>
				<?php echo form_error('contact_number', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="birthday">Birthday</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Birthday','type'=>'text','name'=>'birthday','value'=> set_value('birthday')]);?>
				<?php echo form_error('birthday', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="gender">Gender</label>
				<br/>
				<?php
					$male = array(
						'name'		=> 'gender',
						'id'		=> 'gender',
						'value'		=> 0,
					);
					$female = array(
						'name'		=> 'gender',
						'id'		=> 'gender',
						'value'		=> 1,
					);
					echo form_radio($male) . '  Male &nbsp;&nbsp;&nbsp;';
					echo form_radio($female) . '  Female ';
				?>
				<?php echo form_error('delete_flag', '<div class="error">', '</div>'); ?>
			</div>
			<div class="form-group">
				<label for="company_id_number">Company ID Number</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Company ID Number','type'=>'text','name'=>'company_id_number','value'=> set_value('company_id_number')]);?>
				<?php echo form_error('company_id_number', '<div class="error">', '</div>'); ?>
			</div>

			<button type="submit" class="btn btn-success">Submit</button>
		<?php echo form_close();?>
	</div>
</div>
