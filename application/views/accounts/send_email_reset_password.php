<div class="out-container">
    <div class="login-page">
        <div class="container">
                <div class="tab-pane fade active in" id="forgetPassword">
                    <!-- Reset Form-->
                    <div class="alert alert-warning">
                        <?php echo ($this->session->flashdata('error'))? $this->session->flashdata('error'):'Please input your Email Address to reset your password';?>
                    </div>
                    <?php echo form_open(base_url('accounts/mailForResetPassword'),['role'=>'form']);?>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="text" class="form-control" id="emailaddress" placeholder="Email" name='emailaddress'>
                        </div>
                        <button type="submit" class="btn btn-danger btn-sm">Send</button>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>