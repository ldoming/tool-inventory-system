<div class="out-container">
    <div class="login-page">
        <div class="container">
                <div class="tab-pane fade active in" id="forgetPassword">
                    <!-- Reset Form-->
                    <div class="alert alert-warning">
                        <?php echo ($this->session->flashdata('error'))? $this->session->flashdata('error'):'Please input your new password to reset your password';?>
                    </div>
                    <?php echo form_open(base_url('accounts/resetPassword/'.$token),['role'=>'form']);?>
                        <div class="form-group">
                            <label for="email">New Password</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'New Password','type'=>'password','name'=>'password','value'=> set_value('password')]);?>
                            <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Confirm Password</label>
                            <?php echo form_input(['class'=>'form-control','placeholder'=>'Confirm New Password','type'=>'password','name'=>'confirmpassword','value'=> set_value('confirmpassword')]);?>
                            <?php echo form_error('confirmpassword', '<div class="error">', '</div>'); ?>
                        </div>
                        <button type="submit" class="btn btn-danger btn-sm">Reset Password</button>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>