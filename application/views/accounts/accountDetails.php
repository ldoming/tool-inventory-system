<?php $value = $results[0];?>
<div class="col-sm-6">
	<div class="table-responsive">
		<table class="table">
			<tbody>
				<tr><td><strong>Full Name</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->name;?></td></tr>
				<tr><td><strong>Email</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->email;?></td></tr>
				<tr><td><strong>Password</strong></td></tr>
				<tr>
					<td class="text-right">
						<a href="#" alt="Reset Password" title="Reset Password">
							****************
						</a>
					</td>
				</tr>
				<tr><td><strong>Account Type</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->account_type;?></td></tr>
				<tr><td><strong>Account Status</strong></td></tr>
				<tr><td class="text-right"><?php echo ($value->delete_flag == 0) ? 'Enabled' : 'Disabled';?></td></tr>
				<tr><td><strong>Firstname</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->firstname;?></td></tr>
				<tr><td><strong>Lastname</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->lastname;?></td></tr>
				<tr><td><strong>Contact Number</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->contact_number;?></td></tr>
			</tbody>
		</table>
	</div>
</div>
<div class="col-sm-6">
	<div class="table-responsive">
		<table class="table">
			<tbody>
				<tr><td><strong>Birthday</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->birthday;?></td></tr>
				<tr><td><strong>Gender</strong></td></tr>
				<tr><td class="text-right"><?php echo ($value->gender == 0) ? 'Male' : 'Female';?></td></tr>
				<tr><td><strong>Company ID Number</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->company_id_number;?></td></tr>
				<tr><td><strong>Created Date</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->created_datetime;?></td></tr>
				<tr><td><strong>Updated Date</strong></td></tr>
				<tr><td class="text-right"><?php echo $value->timestamp;?></td></tr>
				<td align="center">
						<?php if($value->delete_flag == 0){?>
						<a href="<?php echo base_url('accounts/accountDelete/' .$value->account_id);?>" title="Disable" alt="Disable" onclick="return confirm('Are you sure you want to disable account <?php echo $value->email;?>?');">
							<i class="fa fa-trash-o fa-3x text-danger"></i>  Disable
						</a>
						<?php }else{?>
						<a href="<?php echo base_url('accounts/accountDelete/' .$value->account_id.'/en');?>" title="Enable" alt="Enable" onclick="return confirm('Are you sure you want to enable account <?php echo $value->email;?>?');">
							<i class="fa fa-recycle fa-3x text-success"></i>  Enable
						</a>
						<?php }?>
						&nbsp;&nbsp;
						<a href="<?php echo base_url('accounts/accountEdit/' .$value->account_id);?>" title="Update" alt="Update">
							<i class="fa fa-pencil-square-o fa-3x text-info"></i>  Update
						</a>
					</td>
			</tbody>
		</table>
	</div>
</div>