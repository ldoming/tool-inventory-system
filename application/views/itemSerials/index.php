<div class="container-fluid" style="margin-bottom:20px;">
	<div class="row-fluid">
		<div class="span12">
			<div class="btn-group">
				<a href="<?php echo base_url('itemSerials/add');?>">
				 	<button class="btn btn-success" type="button">
				 		<i class="fa fa-plus"></i> Add
			 		</button>
		 		</a>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<td>Item</td>
					<td>Serial Number</td>
					<td align="center">Actions</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Sample1</td>
					<td>Sample1</td>
					<td align="center">
						<a href="#" title="Delete" alt="Delete">
							<i class="fa fa-trash-o text-danger"></i>
						</a>
						&nbsp;&nbsp;
						<a href="#" title="Update" alt="Update">
							<i class="fa fa-pencil-square-o text-success"></i>
						</a>
						&nbsp;&nbsp;
						<a href="#" title="Add Serials" alt="Add Serials">
							<i class="fa fa-tasks text-info"></i>
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<nav class="text-center">
	  	<ul class="pagination pagination-centered">
			<li><a href="#"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
	  	</ul>
	</nav>
</div>