-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2014 at 02:17 PM
-- Server version: 5.6.15-log
-- PHP Version: 5.4.9-4ubuntu2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ci_review_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `account_type` varchar(50) NOT NULL DEFAULT 'user',
  `user_token` varchar(255) DEFAULT NULL,
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `email`, `password`, `account_type`, `user_token`, `delete_flag`, `created_datetime`, `timestamp`) VALUES
(1, 'Lymuel Doming', 'lymuel.doming@gmail.com', '05a671c66aefea124cc08b76ea6d30bb', 'user', '20cfed50adf01c9fac2b54d8a36a0721fb469d7ef430b0baf0cab6c436e70375', 0, '2014-09-02 17:04:56', '2014-09-02 09:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL COMMENT 'category foreign key',
  `storage_id` int(11) NOT NULL COMMENT 'storage foreign key',
  `item_type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL COMMENT 'Account foreign key',
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_serial_number`
--

CREATE TABLE IF NOT EXISTS `item_serial_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL COMMENT 'items foreign key',
  `serial_number` varchar(100) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_transactions`
--

CREATE TABLE IF NOT EXISTS `item_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL COMMENT 'items foreign key',
  `borrowed_quantity` int(11) NOT NULL DEFAULT '0',
  `item_serial_number_id` int(11) NOT NULL COMMENT 'items_serial_number foreign key',
  `description` varchar(255) NOT NULL,
  `borrowed_by` int(11) NOT NULL COMMENT 'Account foreign key',
  `approved_by` int(11) NOT NULL COMMENT 'Account foreign key',
  `lend_by` int(11) NOT NULL COMMENT 'Account foreign key',
  `transaction_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 – waiting for approval and 1 – approved',
  `date_reserved` datetime NOT NULL,
  `date_borrowed` datetime NOT NULL,
  `date_returned` datetime NOT NULL,
  `item_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 – good and 1 – defective',
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_infos`
--

CREATE TABLE IF NOT EXISTS `member_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL COMMENT 'Account foreign key',
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `contact_number` int(11) NOT NULL,
  `birthday` int(11) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `company_id_number` varchar(100) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reset_tokens`
--

CREATE TABLE IF NOT EXISTS `reset_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `reset_token` varchar(255) NOT NULL,
  `is_accessed` tinyint(4) NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `reset_tokens`
--

INSERT INTO `reset_tokens` (`id`, `email`, `reset_token`, `is_accessed`, `created_datetime`, `timestamp`) VALUES
(1, 'lymuel.doming@gmail.com', '6219b1e5b0bf94d6c6c06fe18c60f783', 1, '2014-08-20 13:11:12', '2014-08-20 07:45:17'),
(2, 'lymuel.doming@gmail.com', '351d7b588ec97e1e6862ab176eb53932', 1, '2014-08-20 16:17:39', '2014-08-20 08:18:15'),
(3, 'lymuel.doming@gmail.com', '6e6d53a952ce698875540427c054355d', 1, '2014-08-20 17:23:38', '2014-08-20 09:35:43'),
(4, 'lymuel.doming@gmail.com', '5c6bbd54382159acc709663ffb23d279', 1, '2014-08-20 17:34:45', '2014-08-20 09:36:34'),
(5, 'lymuel.doming@bibo.com.ph', '8eb81da8777a14d456d94b488e9ae33e', 1, '2014-08-20 17:36:51', '2014-08-20 09:37:42'),
(6, 'lymuel.doming@gmail.com', 'f9ef18342fc960e2ad8894ffeb2bbf3b', 1, '2014-08-22 16:40:16', '2014-08-22 08:40:43'),
(7, 'lymuel.doming@gmail.com', 'c8314165fc7f42329f65389cdd4f02db', 1, '2014-08-22 16:47:14', '2014-08-22 09:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE IF NOT EXISTS `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
