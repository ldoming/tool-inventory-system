<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	if (!function_exists('item_type'))
	{
		function item_type($key = null)
		{
			if (!$key)
			{
				return null;
			}
			$data = [
				'0' => 'Consumable',
				'1' => 'Returnable',
			];
			if(isset($data[$key]))
			{
				return $data[$key];
			}
			else
			{
				return '-';
			}
		}
	}

	if (!function_exists('item_status'))
	{
		function item_status($key = null)
		{
			if (!$key)
			{
				return null;
			}
			$data = [
				'0' => 'Out of Stocks',
				'1' => 'Have Stocks',
			];
			if(isset($data[$key]))
			{
				return $data[$key];
			}
			else
			{
				return '-';
			}
		}
	}

	if (!function_exists('gender'))
	{
		function gender($key = null)
		{
			if ($key === null)
			{
				return null;
			}
			$data = [
				'0' => 'Male',
				'1' => 'Female',
			];
			if(isset($data[$key]))
			{
				return $data[$key];
			}
			else
			{
				return '-';
			}
		}
	}

	if (!function_exists('date_to_words'))
	{
		function date_to_words($date = null)
		{
			$d = DateTime::createFromFormat('Y-m-d', $date);
			if ($d && $d->format('Y-m-d') == $date)
			{
				return date("F j, Y",strtotime($date));
			}
			return $date;
		}
	}