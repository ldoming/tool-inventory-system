<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function init_pagination($url, $total_rows, $per_page = 10, $segment = 3){
	$ci 									=& get_instance();
	$config['base_url'] 					= $url;
	$config['per_page'] 					= $per_page;
	$config['total_rows'] 					= $total_rows;
	$config["uri_segment"] 					= $segment;
	$config['prev_link'] 					= 'Prev';
	$config['prev_tag_open'] 				= '<li>';
	$config['prev_tag_close'] 				= '</li>';
	$config['next_link'] 					= 'Next';
	$config['next_tag_open'] 				= '<li>';
	$config['next_tag_close'] 				= '</li>';
	$config['num_tag_open'] 				= '<li>';
	$config['num_tag_close'] 				= '</li>';
	$config['cur_tag_open'] 				= '<li class="active"><a href="#">';
	$config['cur_tag_close'] 				= '</a></li>';
	$config['use_page_numbers'] 			= TRUE;
	$config['page_query_string'] 			= TRUE;
	$config['enable_query_strings']			= TRUE;
	$config['query_string_segment'] 		= 'page';
	$ci->pagination->initialize($config);
	return $config;
}