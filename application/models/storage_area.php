<?php

class storage_area extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function retreiveStorageAreaLists()
	{
		$query = $this->db->get('storage');
		return $query->result();
	}

	public function retreiveStorageType()
	{
		$this->db->select('id, name');
		$this->db->order_by('name','ASC');
		$query = $this->db->get('storage');
		$type[''] = '-- Please Select --';
		foreach ($query->result() as $value) {
			$type[$value->id] = $value->name;
		}
		return $type;
	}

	public function insertStorageAreaInfo($info = array())
	{
		$data = array(
			'name' 				=> $info['name'],
			'delete_flag' 		=> $info['delete_flag'],
			'description' 		=> $info['description'],
			'created_datetime'	=> date('Y-m-d h:i:s'),
		);
		return $this->db->insert('storage', $data);
	}

}