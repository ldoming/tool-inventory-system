<?php


class account extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function checkAccountIfExist($emailaddress = null)
	{
		if(!$emailaddress)
		{ 
			return false;
		}
		$result = $this->db->get_where('accounts',['email' => $emailaddress]);
		if($result->result())
		{
			return true;
		}
		return false;
	}

	public function insertTokenInformation($email = null, $token = null)
	{
		if(!$email || !$token)
		{
			return false;
		}
		# Insert token info to database
		$data = [
			'email' => $email,
			'reset_token' => $token,
			'created_datetime' => date('Y-m-d H:i:s')
		];
		$result = $this->db->insert('reset_tokens', $data);
		if($result)
		{
			return send_email($email, 'Link to reset password', base_url('accounts/resetPassword/'.$token));
		}
		return false;
	}

	public function checkTokenIfExist($token = null)
	{
		$result = $this->db->get_where('reset_tokens',['reset_token'=>$token , 'is_accessed' => 0]);
		if($result->result())
		{
			return $result->result()[0]->email;
		}
		return false;
	}

	public function updateTokenToAccessed($token = null)
	{
		$data = array(
			   'is_accessed' => true,
			   'timestamp' => date('Y-m-d H:i:s')
			);

		$this->db->where('reset_token', $token);
		return $this->db->update('reset_tokens', $data); 
	}

	public function resetPassword($password,$email)
	{
		$data = array(
			   'password' => md5($password)
			);
		$this->db->where('email', $email);
		return $this->db->update('accounts', $data); 
	}

	public function registerAccount($data)
	{
		$account = [
			'name' => $data['fullname'],
			'email' => $data['email'],
			'password' => md5($data['password']),
			'created_datetime' => date('Y-m-d H:i:s')
		];
		return $this->db->insert('accounts', $account);
	}

	public function accountLogin($data)
	{
		$result = $this->db->get_where('accounts', array('email' => $data['email'],'password' => md5($data['password']),'delete_flag' => 1));
		if ($result->result())
		{
			return 'Disabled';
		}
		
		$result = $this->db->get_where('accounts', array('email' => $data['email'],'password' => md5($data['password']),'delete_flag' => 0));
		if($result->result())
		{
			$newdata = [
				'id' => $result->result()[0]->id,
				'name' => $result->result()[0]->name,
				'email' => $result->result()[0]->email,
				'account_type' => $result->result()[0]->account_type,
				'created_datetime' => $result->result()[0]->created_datetime,
				'timestamp' => $result->result()[0]->timestamp,
				'logged_in' => true
			];
			$this->session->set_userdata($newdata);
			return $this->session->userdata('logged_in');
		}
		return false;
	}

	public function logout()
	{
		$newdata = [
			'id' => '',
			'name' => '',
			'email' => '',
			'created_datetime' => '',
			'timestamp' => '',
			'logged_in' => false
		];
		$this->session->unset_userdata($newdata);
		$this->session->sess_destroy();
		if(!$this->session->all_userdata())
		{
			return true;
		}
		return false;
	}

	public function retreiveAccounts($returnType = 'data', $limit = 20, $offset = 0, $conditions = null)
	{
		if (!isset($conditions['disabled']))
		{
			$this->db->where('delete_flag', false);
		}

		$account_type = array();
		if (isset($conditions['user']) && $conditions['user'] == 1)
		{
			$account_type[] = 'user';
		}
		if (isset($conditions['operator']) && $conditions['operator'] == 1)
		{
			$account_type[] = 'operator';
		}
		if (isset($conditions['admin']) && $conditions['admin'] == 1)
		{
			$account_type[] = 'admin';
		}
		if (!empty($account_type))
		{
			$this->db->where_in('account_type', $account_type);
		}
		if (isset($conditions['search']) && $conditions['search'] != '')
		{
			$this->db->like('accounts.name', $conditions['search']);
			$this->db->or_like('accounts.email', $conditions['search']);
			$this->db->or_like('member_infos.contact_number', $conditions['search']);
		}

		$this->db->select('accounts.id,accounts.name,
			accounts.email,accounts.account_type,
			member_infos.contact_number,
			member_infos.birthday,member_infos.gender,accounts.delete_flag');
		$this->db->order_by('accounts.name','ASC');
		$this->db->join('member_infos', 'member_infos.account_id = accounts.id','left');
		$query = $this->db->get('accounts', $limit, $offset);

		if ($returnType == 'count')
		{
			return count($query->result());
		}

		return $query->result();
	}

	public function checkAccountIfExistById($id = null)
	{
		if(!$id)
		{ 
			return false;
		}
		$this->db->select('*');
		$this->db->where('accounts.id', $id);
		$this->db->from('member_infos');
		$this->db->join('accounts', 'member_infos.account_id = accounts.id','right');
		$query = $this->db->get();
		if($query->result())
		{
			return $query->result();
		}
		return false;
	}

	public function accountUpdate($value = array())
	{
		$accounts = array(
			'name' 				=> ucwords(strtolower($value['fullname'])),
			'email' 			=> strtolower($value['email']),
			'account_type' 		=> $value['account_type'],
			'delete_flag' 		=> $value['delete_flag'],
		);
		$member_infos = array(
			'firstname' 		=> ucwords(strtolower($value['firstname'])),
			'lastname' 			=> ucwords(strtolower($value['lastname'])),
			'contact_number' 	=> $value['contact_number'],
			'birthday' 			=> $value['birthday'],
			'gender' 			=> $value['gender'],
			'company_id_number' => $value['company_id_number'],
		);
		if ($this->_checkMemberInfoIfExist($value['id']))
		{
			$this->db->where('account_id', $value['id']);
			$this->db->update('member_infos', $member_infos); 
		}
		else
		{
			$member_infos['account_id'] = $value['id'];
			$member_infos['created_datetime'] = date('Y-m-d H:i:s');
			$this->db->insert('member_infos', $member_infos); 
		}
		$this->db->where('id', $value['id']);
		return $this->db->update('accounts', $accounts);
	}

	public function accountAdd($value = array())
	{
		$this->db->trans_start();
		$accounts = array(
			'name' 				=> ucwords(strtolower($value['fullname'])),
			'email' 			=> strtolower($value['email']),
			'password' 			=> md5($value['password']),
			'account_type' 		=> $value['account_type'],
			'delete_flag' 		=> $value['delete_flag'],
			'created_datetime'	=> date('Y-m-d H:i:s'),
		);
		$this->db->insert('accounts', $accounts);
		$insert_id = $this->db->insert_id();

		$member_infos = array(
			'account_id'		=> $insert_id,
			'firstname' 		=> ucwords(strtolower($value['firstname'])),
			'lastname' 			=> ucwords(strtolower($value['lastname'])),
			'contact_number' 	=> $value['contact_number'],
			'birthday' 			=> $value['birthday'],
			'gender' 			=> $value['gender'],
			'company_id_number' => $value['company_id_number'],
			'created_datetime'	=> date('Y-m-d H:i:s'),
		);
		$this->db->insert('member_infos', $member_infos);

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			$this->db->trans_complete();
			return true;
		}
	}

	public function retreiveAccountDetails($id)
	{
		$this->db->select('*');
		$this->db->where('accounts.id',$id);
		$this->db->join('member_infos', 'member_infos.account_id = accounts.id','left');
		$query = $this->db->get('accounts', 1, 0);
		return $query->result();
	}

	public function accountDelete($id,$type)
	{
		$this->db->where('id', $id);
		return $this->db->update('accounts', ['delete_flag' => $type]); 
	}


	########################################################################################
	# Private methods
	########################################################################################
	
	private function _checkMemberInfoIfExist($id = null)
	{
		if(!$id)
		{
			return false;
		}
		$result = $this->db->get_where('member_infos', array('account_id' => $id));
		if (count($result->result()) > 1)
		{
			$this->db->where('account_id', $id);
			$this->db->delete('member_infos');
			return false;
		}
		return $result->result();
	}
	
}