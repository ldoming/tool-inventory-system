<?php

class item extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function insertItemInfo($info = array())
	{
		$data = array(
			'name' 				=> $info['name'],
			'description' 		=> $info['description'],
			'category_id' 		=> $info['category_id'],
			'storage_id' 		=> $info['storage_id'],
			'item_type' 		=> $info['item_type'],
			'status' 			=> $info['status'],
			'quantity' 			=> $info['quantity'],
			'delete_flag' 		=> $info['delete_flag'],
			'image' 			=> '',
			'added_by'			=> $this->session->userdata('id'),
			'created_datetime'	=> date('Y-m-d h:i:s'),
		);
		return $this->db->insert('items', $data);
	}

	public function updateItemInfo($info = array(),$id)
	{
		$data = array(
			'name' 				=> $info['name'],
			'description' 		=> $info['description'],
			'category_id' 		=> $info['category_id'],
			'storage_id' 		=> $info['storage_id'],
			'item_type' 		=> $info['item_type'],
			'status' 			=> $info['status'],
			'quantity' 			=> $info['quantity'],
			'delete_flag' 		=> $info['delete_flag'],
			'image' 			=> '',
			'added_by'			=> $this->session->userdata('id'),
			'created_datetime'	=> date('Y-m-d h:i:s'),
		);
		$this->db->where('id', $id);
		return $this->db->update('items', $data); 
	}

	public function retreiveItemLists($returnType = 'data', $limit = 20, $offset = 0, $conditions = null)
	{
		if (!isset($conditions['disabled']))
		{
			$this->db->where('items.delete_flag', false);
		}
		if (isset($conditions['category_id']) && $conditions['category_id'] != null)
		{
			$this->db->where('items.category_id', $conditions['category_id']);
		}
		if (isset($conditions['storage_id']) && $conditions['storage_id'] != null)
		{
			$this->db->where('items.storage_id', $conditions['storage_id']);
		}

		if (isset($conditions['search']) && $conditions['search'] != '')
		{
			$this->db->like('items.name', $conditions['search']);
			$this->db->or_like('items.quantity', $conditions['search']);
		}

		$this->db->select('items.id AS itemId,items.name AS itemName, items.delete_flag AS itemDeleteFlag, categories.name AS categoryName, storage.name AS storageName, items.item_type, items.status, items.quantity');
		$this->db->join('categories', 'items.category_id = categories.id','left');
		$this->db->join('storage', 'items.storage_id = storage.id','left');
		$this->db->join('accounts', 'items.added_by = accounts.id','left');
		$this->db->order_by('items.name','ASC');
		$query = $this->db->get('items', $limit, $offset);
		if ($returnType == 'count')
		{
			return count($query->result());
		}
		return $query->result();
	}

	public function retreiveItemDetail($id)
	{
		$this->db->select('*');
		$this->db->where('items.id',$id);
		$query = $this->db->get('items', 1, 0);
		return $query->result();
	}

	public function itemDelete($id,$type)
	{
		$this->db->where('id', $id);
		return $this->db->update('items', ['delete_flag' => $type]); 
	}

}