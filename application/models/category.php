<?php

class category extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function retreiveCategoryLists()
	{
		$query = $this->db->get('categories');
		return $query->result();
	}

	public function retreiveCategoryType()
	{
		$this->db->select('id, name');
		$this->db->order_by('name','ASC');
		$query = $this->db->get('categories');
		$type[''] = '-- Please Select --';
		foreach ($query->result() as $value) {
			$type[$value->id] = $value->name;
		}
		return $type;
	}

	public function insertCategoryInfo($info = array())
	{
		$data = array(
			'name' 				=> $info['name'],
			'delete_flag' 		=> $info['delete_flag'],
			'description' 		=> $info['description'],
			'created_datetime'	=> date('Y-m-d h:i:s'),
		);
		return $this->db->insert('categories', $data);
	}

}